package com.captton.programa;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Inicio {

	public static void main(String[] args) {

		String resultado = "";
		int numerador, denominador;
		Scanner scanner = new Scanner(System.in);
		
		try {
		System.out.println("Ingrese el numerador: ");
		numerador = scanner.nextInt();
		System.out.println("Ingrese el denominador: ");
		denominador = scanner.nextInt();
		
		resultado = String.valueOf(numerador / denominador);
		System.out.println("El resultado es " + resultado + ".");
		} catch (ArithmeticException e) {
			// e.printStackTrace();
			System.out.println("No se puede dividir por 0");
		} catch (InputMismatchException e) {
			// e.printStackTrace();
			System.out.println("Por favor, ingrese un n�mero");
		} finally {
			//Casi no usa el finally
		}
		
		scanner.close();
	}
}
